# MySQL install on Ubuntu16
WARNING: This playbook remove old version and all configs!!!!
## Install
### hosts setup
```bash
echo -e '[mysql]\nmysqlhost\n' >> hosts
```

### ansible.cfg example
```bash
cat <<'EOF' > ansible.cfg
[defaults]
hostfile = hosts
remote_user = root
deprecation_warnings = False
roles_path = roles
force_color = 1
retry_files_enabled = False
executable = /bin/bash

[ssh_connection]
ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
pipelining = True
EOF
```
### Install python (ubuntu)
For use ansible, you need install python2.7 on remote server.
```bash
ansible mysql -m raw -a "apt-get -qq update;\
apt-get -y --no-install-recommends install python2.7 python-apt;\
ln -s ./python2.7 /usr/bin/python"
```

### Download ansible role
```bash
mkdir ./roles
cat <<'EOF' > ./roles/req.yml

- src: git+https://gitlab.com/kapuza-ansible/mysql.git
  name: mysql
EOF

echo "roles/mysql" >> .gitignore
ansible-galaxy install --force -r ./roles/req.yml
```

## Use role
```bash
cat << 'EOF' > mysql.yml
---
- name: Install MySQL
  hosts:
    mysql
  roles:
    - role: mysql
      version: '5.7'
      mysql_rootpw: 'secret'
      mysql_remove_anonymous_users: 'Y'
      mysql_disallow_root_login_remotely: 'Y'
      mysql_remove_test_database: 'Y'
      mysql_bind_address: '0.0.0.0'
      mysql_enable_ssl: 'false'
      mysql_LimitNOFILE: 500000
EOF

ansible-playbook mysql.yml
```
